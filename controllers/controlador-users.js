"use strict";

const Joi = require("joi");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const {
  getUserByEmail,
  addUser,
} = require("../repositories/repositorio-usuarios");

async function registerUser(req, res) {
  try {
    const { body } = req;

    const schema = Joi.object({
      nombre: Joi.string().min(4).max(20).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(4).max(20).required(),
      repeatPassword: Joi.ref("password"),
    });

    await schema.validateAsync(body);
    const { nombre, email, password } = body;
    const user = await getUserByEmail(email);

    if (user) {
      const error = new Error("Ya existe un usuario registrado con ese email");
      error.status = 400;
      throw error;
    }

    const contraseñaEncriptada = await bcrypt.hash(password, 10);
    const userId = await addUser({
      nombre: nombre,
      email: email,
      password: contraseñaEncriptada,
      rol: null,
    });

    res.status(200);
    res.send({ userId: userId });
  } catch (error) {
    res.send({ Error: error.message });
  }
}

async function login(req, res) {
  try {
    const { body } = req;

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(4).max(20).required(),
    });

    schema.validateAsync(body);

    const { email, password } = body;
    const user = await getUserByEmail(email);

    if (!user) {
      throw new Error("No existe un usuario con ese email");
    }

    const { id, nombre, password: dbPassword, rol } = user;

    console.log(dbPassword);

    const isValidPassword = await bcrypt.compare(password, dbPassword);

    if (!isValidPassword) {
      throw new Error("Contraseña incorrecta");
    }

    const { JWT_SECRET } = process.env;
    const tokenPayload = { id, nombre, email, rol };
    const token = jwt.sign(tokenPayload, JWT_SECRET);

    res.status = 200;
    res.send({ token: token });
  } catch (error) {
    res.send({ Error: error.message });
  }
}

module.exports = { registerUser, login };
