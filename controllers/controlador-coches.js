"use strict";

const {
  mostrarCoches,
  buscarCochePorId,
  filterCoches,
} = require("../repositories/repositorio-coches");

async function getCoches(req, res) {
  try {
    const coches = await mostrarCoches();

    res.status(200);
    res.send(coches);
  } catch (error) {
    res.send({ Error: error.message });
  }
}

async function getCochePorId(req, res) {
  try {
    const { idCoche } = req.params;
    const coche = await buscarCochePorId(idCoche);

    res.send(coche);
  } catch (error) {
    res.send({ Error: error.message });
  }
}

async function explorarCoches(req, res) {
  try {
    const coches = await filterCoches(req.query);

    res.send(coches);
  } catch (error) {
    res.send({ Error: error.message });
  }
}

module.exports = { getCoches, getCochePorId, explorarCoches };
