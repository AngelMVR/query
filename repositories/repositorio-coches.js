"use strict";

const database = require("../infrastructure/database");

async function mostrarCoches() {
  const pool = await database.getPool();
  const consulta = "SELECT * FROM cars";
  const [coches] = await pool.query(consulta);

  return coches;
}

async function buscarCochePorId(id) {
  const pool = await database.getPool();
  const consulta = "SELECT * FROM cars WHERE cars.id = ?";
  const [coches] = await pool.query(consulta, id);

  return coches[0];
}

async function filterCoches(querys) {
  const { marca, modelo, anho, motor, minCv, maxCv } = querys;

  const pool = await database.getPool();
  const consulta =
    "SELECT * FROM cars WHERE (? IS NULL OR marca = ?) AND (? IS NULL OR modelo = ?) AND (? IS NULL OR anho = ?) AND (? IS NULL OR motor = ?) AND cv BETWEEN IF(? IS NOT NULL, ?, 0) AND IF(? IS NOT NULL, ?, 3000)";
  const [coches] = await pool.query(consulta, [
    marca,
    marca,
    modelo,
    modelo,
    anho,
    anho,
    motor,
    motor,
    minCv,
    minCv,
    maxCv,
    maxCv,
  ]);

  return coches;
}

module.exports = {
  mostrarCoches,
  buscarCochePorId,
  filterCoches,
};
