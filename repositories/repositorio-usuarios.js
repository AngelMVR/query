"use strict";

const database = require("../infrastructure/database");

async function getUserByEmail(email) {
  const pool = await database.getPool();
  const consulta = `SELECT id, email, password
    FROM users
    WHERE email = ?`;
  const [user] = await pool.query(consulta, email);

  return user[0];
}

async function addUser(user) {
  const pool = await database.getPool();
  const consulta = `
      INSERT INTO users(
        nombre,
        email,
        password,
        rol
      ) VALUES (?, ?, ?, ?)
    `;

  const [created] = await pool.query(consulta, [...Object.values(user)]);

  return created.insertId;
}

module.exports = { getUserByEmail, addUser };
