"use strict";

require("dotenv").config();
const express = require("express");
const app = express();
const routerCoches = require("./routes/rutas-coches");
const routerUsers = require("./routes/rutas-usuarios");

const { SERVER_PORT } = process.env;

app.use(express.json());
app.use("/coches", routerCoches);
app.use("/users", routerUsers);

app.listen(SERVER_PORT);
