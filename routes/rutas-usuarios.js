"use strict";

const express = require("express");
const router = express.Router();
const { registerUser, login } = require("../controllers/controlador-users");
const validateAuth = require("../middlewares/validate-auth");

router.route("/register").post(registerUser);
router.route("/login").post(login);

// Test que hicimos para entender validateAuth y por qué guardamos los datos del token en req.auth
router
  .route("/private")
  .all(validateAuth)
  .get((req, res) =>
    res.send(`Esto es privado y el id del usuario es ${req.auth.id}`)
  );

module.exports = router;
