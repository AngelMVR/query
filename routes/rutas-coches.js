"use strict";

const express = require("express");
const router = express.Router();
const {
  getCoches,
  getCochePorId,
  explorarCoches,
} = require("../controllers/controlador-coches");

router.route("/").get(getCoches);
router.route("/explore").get(explorarCoches);
router.route("/:idCoche").get(getCochePorId);

module.exports = router;
