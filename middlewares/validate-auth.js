"use strict";

const jwt = require("jsonwebtoken");

async function validateAuth(req, res, next) {
  try {
    const token = req.headers.authorization;
    const { JWT_SECRET } = process.env;

    const decodedToken = jwt.verify(token, JWT_SECRET);

    req.auth = decodedToken;

    next();
  } catch (error) {
    res.send({ Error: "Usuario no autorizado para esta ruta" });
  }
}

module.exports = validateAuth;
