CREATE DATABASE IF NOT EXISTS cars_reviews;

USE cars_reviews;

CREATE TABLE IF NOT EXISTS cars (
	id INT NOT NULL AUTO_INCREMENT,
    marca VARCHAR(100) NOT NULL,
    modelo VARCHAR(255) NOT NULL,
    anho INT NOT NULL,
    motor ENUM('Diésel', 'Gasolina', 'Híbrido', 'Eléctrico') DEFAULT 'Gasolina',
    cv INT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users (
	id INT NOT NULL AUTO_INCREMENT,
    nombre varchar(255) NOT NULL,
    email varchar(250) NOT NULL,
    password VARCHAR(128) NOT NULL,
    rol ENUM('admin', 'reader') DEFAULT 'reader',
    image VARCHAR(255) NULL,
	PRIMARY KEY (id)
);

INSERT INTO cars (marca, modelo, anho, motor, cv) VALUES ("Mazda", "3", "2019", "Gasolina", 120);
INSERT INTO cars (marca, modelo, anho, motor, cv) VALUES ("Renault", "Megane", "2018", "Diesel", 110);
INSERT INTO cars (marca, modelo, anho, motor, cv) VALUES ("Seat", "Leon", "2021", "Gasolina", 90);
INSERT INTO cars (marca, modelo, anho, motor, cv) VALUES ("Seat", "Ateca", "2017", "Diesel", 150);